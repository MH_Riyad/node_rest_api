const express = require('express');
const bodyParser = require('body-parser') ;
const mongoose = require('mongoose') ;
const app = express() ;

// db connect 
const db = require('./config/keys').mongodbURI ;
mongoose.connect(db,{ useNewUrlParser: true})
.then(()=>console.log('db is connected'))
.catch((error)=> console.log(error)) ;

mongoose.set('useFindAndModify', false);
// body-parser middleware
app.use(bodyParser.json()) ;
app.use(bodyParser.urlencoded({ extended: true }));

// initilize routes
app.use('/api',require('./routes/api')) ;

// error handling middleware
app.use((err, req, res, next)=>{
    console.log(err);
    res.status(422).send({ error: err.message }) ;
}) ;

// set Port 
const PORT = process.env.PORT || 3000 ;

app.listen(PORT, ()=> console.log(`app is listining to ${PORT}`) );