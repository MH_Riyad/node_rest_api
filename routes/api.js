const router = require('express').Router();
const Ninja = require('../models/ninja');

router.get('/ninjas', (req, res) => {
    res.send({
        name: 'riyad'
    });
});

router.post('/ninjas', (req, res, next) => {

    Ninja.create(req.body)
        .then(ninja => res.send(ninja))
        .catch(next);

});
router.put('/ninjas/:id', (req, res, next) => {
    Ninja.findByIdAndUpdate({
            _id: req.params.id
        }, req.body)
        .then(() => {
            console.log('find') ;
            Ninja.findOne({
                    _id: req.params.id
                })
                .then(ninja => res.send(ninja))
        })
        .catch(next);
});
router.delete('/ninjas/:id', (req, res, next) => {
    Ninja.findByIdAndRemove({
            _id: req.params.id
        })
        .then(ninja => res.send(ninja))
        .catch(next);
});

module.exports = router;